<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
    * Seed the application's database.
    *
    * @return void
    */
    public function run()
    {
        // Quando viene chiamato il seed, pulisco il database prima di popolarlo, così da non duplicare i dati.
        DB::table('airports')->delete();
        
        // Creo l'array multidimensionale degli aeroporti.
        $airports = [
            [
                'code' => 'MXP',
                'name' => 'Milano - Malpensa',
                'lat' => '45.6286572',
                'lng' => '8.713936'
            ],
            [
                'code' => 'FLR',
                'name' => 'Firenze - Amerigo Vespucci',
                'lat' => '43.8086573',
                'lng' => '11.199036'
            ],
            [
                'code' => 'FCO',
                'name' => 'Roma - Fiumicino',
                'lat' => '41.8034314',
                'lng' => '12.2497041'
            ],
            [
                'code' => 'PMO',
                'name' => 'Palermo - Falcone e Borsellino',
                'lat' => '38.17955',
                'lng' => '13.0967716'
            ],
            [
                'code' => 'NCE',
                'name' => 'Nice - Cote d\'azure',
                'lat' => '43.6586004',
                'lng' => '7.2022872'
            ],
            [
                'code' => 'MAD',
                'name' => 'Madrid - Barajas',
                'lat' => '40.4983363',
                'lng' => '-3.5697869'
            ],
            [
                'code' => 'LIS',
                'name' => 'Lisbona - Portela',
                'lat' => '38.7755978',
                'lng' => '-9.1375554'
            ],
            [
                'code' => 'LGW',
                'name' => 'London - Gatwick',
                'lat' => '51.1536654',
                'lng' => '-0.1842516'
            ],
            [
                'code' => 'CDG',
                'name' => 'Paris - Charles de Gaulle',
                'lat' => '49.0080748',
                'lng' => '2.5487556'
            ],
            [
                'code' => 'AMS',
                'name' => 'Amsterdam - Schiphol',
                'lat' => '52.3105419',
                'lng' => '4.7660857'
            ],
            [
                'code' => 'BER',
                'name' => 'Berlin - Brandenburg Airport',
                'lat' => '52.3647137',
                'lng' => '13.5088785'
            ],
            [
                'code' => 'ARN',
                'name' => 'Stockholm - Arlanda',
                'lat' => '59.6497649',
                'lng' => '17.921592'
            ],
            [
                'code' => 'ATH',
                'name' => 'Athens - Eleftherios Venizelos',
                'lat' => '37.9356509',
                'lng' => '23.9462269'
            ],
            [
                'code' => 'JFK',
                'name' => 'New York - JFK',
                'lat' => '40.6413153',
                'lng' => '-73.780327'
            ],
            [
                'code' => 'LAX',
                'name' => 'Los Angeles - LA International Airport',
                'lat' => '33.9415933',
                'lng' => '-118.4107187'
            ],
            [
                'code' => 'MIA',
                'name' => 'Miami - Miami International Airport',
                'lat' => '25.7958771',
                'lng' => '-80.2892396'
            ],
            [
                'code' => 'PEK',
                'name' => 'Beijing - Capital International Airport',
                'lat' => '40.0798614',
                'lng' => '116.6009234'
            ],
            [
                'code' => 'HND',
                'name' => 'Tokyo - Tokyo-Haneda Airport',
                'lat' => '35.5493975',
                'lng' => '139.7753539'
            ],
            [
                'code' => 'SYD',
                'name' => 'Sydney - Kingsford Smith International Airport',
                'lat' => '-33.9500299',
                'lng' => '151.1797237'
            ],
        ];
        
        // Inserisco l'array multidimensionale $airports nella tabella del DB.
        DB::table('airports')->insert($airports);
        
        // Restituisco un messaggio di conferma di avvenuto inserimento dei dati degli aeroporti nel DB.
        $this->command->info('La lista degli aeroporti è stata correttamente importata nel database.');
        
        // Ora definisco l'array vuoto dei voli disponibili.
        $flights = [];
        
        // Creo 500 voli random utilizzando come dati l'array dei codici degli aeroporti $airport_codes e prezzi casuali.
        for ($i=0; $i<500; $i++) {
            
            // Creo un array che conterrà i codici degli aeroporti:
            $airport_codes=[];
            
            foreach ($airports as $airport) {
                array_push($airport_codes, $airport['code']);
            }
            
            // Prendo un codice di aeroporto casuale come aeroporto di partenza:
            $origin_airport = $airport_codes[array_rand($airport_codes)];
            
            // Modifico l'array contenente i codici degli aeroporti togliendo quello di partenza, in modo da non riselezionarlo come quello di arrivo.
            if (($code = array_search($origin_airport, $airport_codes)) !== false) {
                unset($airport_codes[$code]);
            }
            
            // Inserisco nell'array vuoto $flights il volo.
            array_push($flights,
            [
                'code_departure' => $origin_airport,
                'code_arrival' => $airport_codes[array_rand($airport_codes)],
                'price' => random_int(50,800)
            ]);
            
            
        }
        
        DB::table('flights')->insert($flights);
        
        // Restituisco un messaggio di conferma di avvenuto inserimento dei dati dei voli nel DB.
        $this->command->info('La lista dei voli è stata correttamente importata nel database.');
    }
}