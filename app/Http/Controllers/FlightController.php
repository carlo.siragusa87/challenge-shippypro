<?php

namespace App\Http\Controllers;

use App\Models\Flight;
use App\Models\Airport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FlightController extends Controller
{
    
    public function getFlights(Request $request) {
        
        // Valido gli input dell'utente, restituendo messaggi custom in caso di errore.
        $validator = Validator::make($request->all(), [
            'partenza' => 'int|different:destinazione',
            'destinazione' => 'int|different:partenza',
        ], $messages = [
            'integer' => 'Per favore, seleziona un aeroporto di :attribute.',
            'different' => 'Non è possibile effettuare la ricerca se la località di partenza e quella di destinazione coincidono.'
        ])->validate();
        
        //Ottengo tutti gli aeroporti e tutti i voli dal database.
        $airports = Airport::all();
        $flights = Flight::all();
        
        //Ottengo gli aeroporti di partenza e destinazione corrispondenti alle selezioni dell'utente.
        $origin = Airport::where('id', $request->input('partenza'))->get();
        $destination = Airport::where('id',$request->input('destinazione'))->get();
        
        //Creo due variabili che contengono tutti gli aerei che partono da $origin e tutti gli aerei che atterrano a $destination rispettivamente.
        $flights_from_origin = collect($flights)->where('code_departure',$origin[0]->code)->all();
        $flights_to_destination = collect($flights)->where('code_arrival',$destination[0]->code)->all();

        // Tutti i voli diretti:
        $no_stopovers_flights = collect($flights)->where('code_departure',$origin[0]->code)->where('code_arrival',$destination[0]->code)->all();

        // Possibili scali:        
        // Tutti i voli che partono da $origin, ma non atterrano a $destination.
        $possible_stopovers_from_origin = collect($flights_from_origin)->where('code_arrival', '!=', $destination[0]->code)->all();
        
        //tutti i voli che atterrano a $destination, ma non partono da $origin.
        $possible_stopovers_to_destination = collect($flights_to_destination)->where('code_departure', '!=', $origin[0]->code)->all(); 
        
        // Array contenente i voli diretti ed il relativo prezzo.
        $direct_flights = [];
        foreach($no_stopovers_flights as $i) {
            $direct_flight = array(
                'flight_1'=>$i,
                'price'=>$i->price
            );
            array_push($direct_flights, $direct_flight);
        }      
        
        // Voli con 1 scalo:
        $to_destination_from = []; //da quali aeroporti partono gli aerei che atterrano a $destination?
        foreach($possible_stopovers_to_destination as $stopover) {
            array_push($to_destination_from, $stopover->code_departure);
        }
        $to_destination_from = array_unique($to_destination_from); 
        
        $to_last_stopover_from_origin = []; // quali aerei partono da $origin verso gli aeroporti che offrono scalo a $destination?
        foreach($possible_stopovers_from_origin as $stopover_origin) {
            if(collect($to_destination_from)->contains($stopover_origin->code_arrival)) {
                array_push($to_last_stopover_from_origin, $stopover_origin);
            };
        };
        
        $to_destination_from_last_stopover = []; // quali aerei partono dagli aeroporti che offrono scalo a $destination?
        foreach($to_destination_from as $stopover_destination) {
            $last_stopover = collect($flights)->where('code_departure', $stopover_destination)->where('code_arrival', $destination[0]->code)->all();
            foreach($last_stopover as $last_flight) {
                array_push($to_destination_from_last_stopover, $last_flight);            
            }
        };
        
        $one_stopover_flights = []; //Array dei voli con 1 scalo, con il prezzo totale del volo.
        foreach ($to_destination_from_last_stopover as $value) {
            $matching_last_flight = $value;
            foreach($to_last_stopover_from_origin as $v) {
                $matching_first_flight = $v;
                if($matching_first_flight && $matching_last_flight->code_departure == $matching_first_flight->code_arrival) {
                    $matching_flights = array(
                        'flight_1'=>$matching_first_flight,
                        'flight_2'=>$matching_last_flight,
                        'price'=>$matching_first_flight->price+$matching_last_flight->price
                    );
                    array_push($one_stopover_flights, $matching_flights);
                }
            }
        }
        
        $two_stopovers_flights = []; //Array dei voli con 2 scali, con il prezzo totale del volo.
        foreach ($to_destination_from_last_stopover as $value) {
            $matching_last_flight = $value;
            
            foreach ($possible_stopovers_from_origin as $v) {
                $matching_first_flight = $v;
                $possible_second_flights = collect($flights)->where('code_departure', $matching_first_flight->code_arrival)->where('code_arrival', $matching_last_flight->code_departure)->values();
                if($matching_first_flight && $matching_last_flight) {      
                    
                    foreach($flights as $j => $w) {
                        $matching_second_flight = $possible_second_flights->get($j);
                        
                        if($matching_second_flight) {     
                            $matching_flights = array(
                                'flight_1'=>$matching_first_flight,
                                'flight_2'=>$matching_second_flight,
                                'flight_3'=>$matching_last_flight,
                                'price'=>$matching_first_flight->price+$matching_second_flight->price+$matching_last_flight->price
                            );
                            array_push($two_stopovers_flights, $matching_flights);
                        }
                    }
                }
            }
        }
        
        //Arrays contenenti i primi 5 voli per ogni "categoria" (diretto o con scali), in ordine di prezzo.
        $cheapest_direct_flights = collect($direct_flights)->sortBy('price')->take(5);
        $cheapest_1_stopover_flights = collect($one_stopover_flights)->sortBy('price')->take(5);
        $cheapest_2_stopovers_flights = collect($two_stopovers_flights)->sortBy('price')->take(5);
        
        return view('search-flights', compact('airports', 'origin', 'destination', 'cheapest_direct_flights', 'cheapest_1_stopover_flights', 'cheapest_2_stopovers_flights'));
        
    }   
}

