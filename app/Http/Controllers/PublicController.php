<?php

namespace App\Http\Controllers;

use App\Models\Airport;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function home() {
        $airports = Airport::all();
        return view('homepage', compact('airports'));
    }
}
