<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FlightController;
use App\Http\Controllers\PublicController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Rotte Pubbliche
Route::get('/', [PublicController::class, 'home'])->name('home');

// Rotte Ricerca Voli
Route::get('/flights/request', [FlightController::class, 'getFlights'])->name('flights.request');

