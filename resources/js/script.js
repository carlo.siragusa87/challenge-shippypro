// Ottengo le card contenenti i voli con il prezzo più basso per categoria.
const DIRECT_CARD = document.querySelector('#cheapest-direct');
const ONE_STOPOVER_CARD = document.querySelector('#cheapest-1-stopover');
const TWO_STOPOVER_CARD = document.querySelector('#cheapest-2-stopovers');

// Ottengo il container dei prezzi dalle card di cui sopra. Se la card non esiste, restituirà null.
const DIRECT_PRICE_CONTAINER = DIRECT_CARD !== null ? DIRECT_CARD.querySelector('.price-container') : null;
const ONE_STOPOVER_PRICE_CONTAINER = ONE_STOPOVER_PRICE_CONTAINER !== null ? ONE_STOPOVER_CARD.querySelector('.price-container') : null;
const TWO_STOPOVER_PRICE_CONTAINER = TWO_STOPOVER_PRICE_CONTAINER !== null ? TWO_STOPOVER_CARD.querySelector('.price-container') : null;

// Ottengo i prezzi contenuti nei container di cui sopra, se il container esiste. Se il container non esiste, il prezzo sarà null.
let directFlightPrice = DIRECT_PRICE_CONTAINER !== null ? DIRECT_PRICE_CONTAINER.querySelector('.price').innerHTML.replace('€', '') : null;
let oneStopoverPrice = ONE_STOPOVER_PRICE_CONTAINER !== null ? ONE_STOPOVER_PRICE_CONTAINER.querySelector('.price').innerHTML.replace('€', '') : null;
let twoStopoversPrice = TWO_STOPOVER_PRICE_CONTAINER !== null ? TWO_STOPOVER_PRICE_CONTAINER.querySelector('.price').innerHTML.replace('€', '') : null;

// Creo un array contenente i prezzi, ed ottengo il prezzo minimo. Se nell'array sarà presente un valore null, tale valore verrà filtrato ed eliminato.
let pricesArray = Array(directFlightPrice, oneStopoverPrice, twoStopoversPrice);
let minPrice = Math.min(...pricesArray.filter(price => price !== null));

// Creo un paragrafo e gli assegno la classe 'best-offer'. Tale classe verrà utilizzata in CSS per assegnare stile al paragrafo.
const BEST_OFFER_PARAGRAPH = document.createElement("p");
const BEST_OFFER_TEXT = document.createTextNode("Best offer!");
BEST_OFFER_PARAGRAPH.appendChild(BEST_OFFER_TEXT);
BEST_OFFER_PARAGRAPH.className = 'best-offer';

//Applico questa logica all'array dei prezzi, in modo da assegnare il paragrafo della migliore offerta alla card dei risultati corretta.
if(minPrice == directFlightPrice) {
    DIRECT_PRICE_CONTAINER.appendChild(BEST_OFFER_PARAGRAPH);
    DIRECT_CARD.classList.add('best-offer-card');
} else if (minPrice == oneStopoverPrice) {
    ONE_STOPOVER_PRICE_CONTAINER.appendChild(BEST_OFFER_PARAGRAPH);
    ONE_STOPOVER_CARD.classList.add('best-offer-card');
} else {
    TWO_STOPOVER_PRICE_CONTAINER.appendChild(BEST_OFFER_PARAGRAPH);
    TWO_STOPOVER_CARD.classList.add('best-offer-card');
}
