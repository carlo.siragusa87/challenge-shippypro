<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script src="{{ asset('js/app.js') }}" defer></script>
    
    <title>Challenge ShippyPro</title>
</head>
<body>
    <main>
        
        <div class="container-fluid">
            <div id="search-container" class="row min-vh-100 py-5">
                <div class="col-12 d-flex justify-content-center align-items-center">
                    
                    <form id="ricerca-voli" class="px-5 py-4 d-flex flex-column justify-content-center align-items-center" method="GET" action="{{ route('flights.request') }}">
                        @csrf
                        <p id="cta-title" class="fs-2 me-auto text-white">Prenota il tuo prossimo viaggio</p>
                        <div class="input-group mb-1">
                            <span class="input-group-text">Partenza</span>
                            <select id="origin-select" name="partenza" class="form-select @error('partenza') is-invalid @enderror" aria-label="Aeroporto di partenza">
                                <option>Scegli l'aeroporto di partenza</option>
                                @foreach ($airports as $airport)
                                <option value="{{ $airport->id }}" @if (old('partenza') == $airport->id) selected="selected" @endif>{{ $airport->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('partenza')
                        <small class="m-0 mt-1 fst-italic text-danger bg-white rounded px-2 py-1">{{ $message }}</small>
                        @enderror
                        
                        <div class="input-group mt-3 mb-1">
                            <span class="input-group-text">Destinazione</span>
                            <select id="destination-select" name="destinazione" class="form-select @error('destinazione') is-invalid @enderror" aria-label="Aeroporto di destinazione">
                                <option>Scegli l'aeroporto di destinazione</option>
                                @foreach ($airports as $airport)
                                <option value="{{ $airport->id }}" @if (old('destinazione') == $airport->id) selected="selected" @endif>{{ $airport->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('destinazione')
                        <small class="m-0 mt-1 fst-italic text-danger bg-white rounded px-2 py-1">{{ $message }}</small>
                        @enderror
                        <button type="submit" class="btn btn-light px-5 mt-3">Ricerca</button>
                        
                    </form>
                    
                </div>
            </div>
        </div>
        
        
    </main>

</body>
</html>