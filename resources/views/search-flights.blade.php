<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script src="{{ asset('js/app.js') }}" defer></script>
    
    <title>Challenge ShippyPro</title>
</head>
<body>
    <main>
        <div class="container-fluid">
            <div id="search-container" class="row py-4">
                <div class="col-12 col-md-6 d-flex justify-content-center align-items-center">
                    <form id="ricerca-voli" class="px-5 py-4 d-flex flex-column justify-content-center align-items-center" method="GET" action="{{ route('flights.request') }}">
                        @csrf
                        <p id="cta-title" class="fs-2 me-auto text-white">Prenota il tuo prossimo viaggio</p>
                        <div class="input-group mb-1">
                            <span class="input-group-text">Partenza</span>
                            <select id="origin-select" name="partenza" class="form-select @error('partenza') is-invalid @enderror" aria-label="Aeroporto di partenza">
                                <option>Scegli l'aeroporto di partenza</option>
                                @foreach ($airports as $airport)
                                <option value="{{ $airport->id }}" @if(old('partenza') == $airport->id) selected="selected" @endif>{{ $airport->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('partenza')
                        <small class="m-0 mt-1 fst-italic text-danger bg-white rounded px-2 py-1">{{ $message }}</small>
                        @enderror
                        
                        <div class="input-group mt-3 mb-1">
                            <span class="input-group-text">Destinazione</span>
                            <select id="destination-select" name="destinazione" class="form-select @error('destinazione') is-invalid @enderror" aria-label="Aeroporto di destinazione">
                                <option>Scegli l'aeroporto di destinazione</option>
                                @foreach ($airports as $airport)
                                <option value="{{ $airport->id }}" @if(old('destinazione') == $airport->id) selected="selected" @endif>{{ $airport->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('destinazione')
                        <small class="m-0 mt-1 fst-italic text-danger bg-white rounded px-2 py-1">{{ $message }}</small>
                        @enderror
                        <button type="submit" class="btn btn-light px-5 mt-3">Ricerca</button>
                    </form>
                    
                </div>
                <div class="col-12 col-md-6 mt-4 mt-md-0 d-flex justify-content-center align-items-center">
                    
                    <iframe width="600" height="350" frameborder="0" style="border:0" referrerpolicy="no-referrer-when-downgrade" src="https://www.google.com/maps/embed/v1/directions?key=AIzaSyDF0_FwgYPKIMw4Q_4bycOU-tMVicEd3Rs&origin={{ $origin->pluck('lat')->first() }},{{ $origin->pluck('lng')->first() }}&destination={{ $destination->pluck('lat')->first() }},{{ $destination->pluck('lng')->first() }}&mode=flying"allowfullscreen>
                    </iframe>
                    
                </div>
            </div>
        </div>
        
        @if($origin && $destination)
        <div class="container">
            <div class="row">
                <div class="col-12 py-4 border-bottom border-1">
                    <strong>
                        <p class="fs-5 m-0">
                            <i class="mx-2 text-secondary fa-solid fa-plane-departure"></i>
                            {{ $origin->pluck('name')->first() }}
                            <i class="ms-5 me-2 text-secondary fa-solid fa-plane-arrival"></i>
                            {{ $destination->pluck('name')->first() }}
                        </p>
                    </strong>
                </div>
            </div>
            <div class="row pt-4">
                <div class="col-12 col-md-4">
                    
                    <p class="fs-3">Voli diretti</p>
                    
                    @if($cheapest_direct_flights && $cheapest_direct_flights->count() > 0)
                    <ol class="p-0 list-unstyled">
                        @foreach ($cheapest_direct_flights as $flight)
                        <li>
                            <div {{ $loop->first ? 'id=cheapest-direct' : ''}} class="bg-light mb-3 rounded-5 py-3 px-4 search-result">
                                <p class="my-0">
                                    <strong>
                                        {{ $flight['flight_1']['code_departure'] }}
                                    </strong><span class="ms-2 text-secondary">────<i class="ms-1 me-2 fa-solid fa-plane fa-xs"></i></span><strong>
                                        {{ $flight['flight_1']['code_arrival'] }}
                                    </strong>
                                </p>
                                <p class="mb-3">
                                    <small>
                                        {{ $airports->where('code',$flight['flight_1']['code_departure'])->pluck('name')->first() }}
                                        ➝
                                        {{ $airports->where('code',$flight['flight_1']['code_arrival'])->pluck('name')->first() }}
                                    </small>
                                </p>
                                
                                <div class="{{ $loop->first ? 'price-container' : ''}} d-flex align-items-center mt-3">
                                    <p class="price fs-4 m-0">{{ $flight['price'] }}€</p>
                                </div>
                                
                            </div>
                        </li>
                        @endforeach
                    </ol>
                    @else
                    <p><i class="fa-solid fa-circle-exclamation me-2"></i>Nessun volo diretto</p>
                    @endif
                    
                </div>
                <div class="col-12 col-md-4">
                    
                    <p class="fs-3">Voli con 1 scalo</p>
                    @if($cheapest_1_stopover_flights && $cheapest_1_stopover_flights->count() > 0)
                    <ol class="p-0 list-unstyled">
                        @foreach ($cheapest_1_stopover_flights as $flight)
                        <li>
                            <div {{ $loop->first ? 'id=cheapest-1-stopover' : ''}} class="bg-light mb-3 rounded-5 py-3 px-4 search-result">
                                <p class="my-0">
                                    <strong>
                                        {{ $flight['flight_1']['code_departure'] }}
                                    </strong><span class="ms-2 text-secondary">────<i class="ms-1 me-2 fa-solid fa-plane fa-xs"></i></span><strong>
                                        {{ $flight['flight_1']['code_arrival'] }}
                                    </strong>
                                </p>
                                <p class="mb-3">
                                    <small>
                                        {{ $airports->where('code',$flight['flight_1']['code_departure'])->pluck('name')->first() }}
                                        ➝
                                        {{ $airports->where('code',$flight['flight_1']['code_arrival'])->pluck('name')->first() }}
                                    </small>
                                </p>
                                
                                <p class="my-0">
                                    <strong>
                                        {{ $flight['flight_2']['code_departure'] }}
                                    </strong><span class="ms-2 text-secondary">────<i class="ms-1 me-2 fa-solid fa-plane fa-xs"></i></span><strong>
                                        {{ $flight['flight_2']['code_arrival'] }}
                                    </strong>
                                </p>
                                <p class="mb-3">
                                    <small>
                                        {{ $airports->where('code',$flight['flight_2']['code_departure'])->pluck('name')->first() }}
                                        ➝
                                        {{ $airports->where('code',$flight['flight_2']['code_arrival'])->pluck('name')->first() }}
                                    </small>
                                </p>
                                
                                <div class="{{ $loop->first ? 'price-container' : ''}} d-flex align-items-center mt-3">
                                    <p class="price fs-4 m-0">{{ $flight['price'] }}€</p>
                                </div>
                                
                            </div>
                        </li>
                        @endforeach
                    </ol>
                    @else
                    <p><i class="fa-solid fa-circle-exclamation me-2"></i>Nessun volo con 1 scalo</p>
                    @endif
                    
                </div>
                <div class="col-12 col-md-4"> 
                    
                    <p class="fs-3">Voli con 2 scali</p>
                    @if($cheapest_2_stopovers_flights && $cheapest_2_stopovers_flights->count() > 0)
                    <ol class="p-0 list-unstyled">
                        @foreach ($cheapest_2_stopovers_flights as $flight)
                        
                        <li>
                            <div {{ $loop->first ? 'id=cheapest-2-stopovers' : ''}} class="bg-light mb-3 rounded-5 py-3 px-4 search-result">
                                <p class="my-0">
                                    <strong>
                                        {{ $flight['flight_1']['code_departure'] }}
                                    </strong><span class="ms-2 text-secondary">────<i class="ms-1 me-2 fa-solid fa-plane fa-xs"></i></span><strong>
                                        {{ $flight['flight_1']['code_arrival'] }}
                                    </strong>
                                </p>
                                <p class="mb-3">
                                    <small>
                                        {{ $airports->where('code',$flight['flight_1']['code_departure'])->pluck('name')->first() }}
                                        ➝
                                        {{ $airports->where('code',$flight['flight_1']['code_arrival'])->pluck('name')->first() }}
                                    </small>
                                </p>
                                
                                <p class="my-0">
                                    <strong>
                                        {{ $flight['flight_2']['code_departure'] }}
                                    </strong><span class="ms-2 text-secondary">────<i class="ms-1 me-2 fa-solid fa-plane fa-xs"></i></span><strong>
                                        {{ $flight['flight_2']['code_arrival'] }}
                                    </strong>
                                </p>
                                <p class="mb-3">
                                    <small>
                                        {{ $airports->where('code',$flight['flight_2']['code_departure'])->pluck('name')->first() }}
                                        ➝
                                        {{ $airports->where('code',$flight['flight_2']['code_arrival'])->pluck('name')->first() }}
                                    </small>
                                </p>
                                
                                <p class="my-0">
                                    <strong>
                                        {{ $flight['flight_3']['code_departure'] }}
                                    </strong><span class="ms-2 text-secondary">────<i class="ms-1 me-2 fa-solid fa-plane fa-xs"></i></span><strong>
                                        {{ $flight['flight_3']['code_arrival'] }}
                                    </strong>
                                </p>
                                <p class="mb-3">
                                    <small>
                                        {{ $airports->where('code',$flight['flight_3']['code_departure'])->pluck('name')->first() }}
                                        ➝
                                        {{ $airports->where('code',$flight['flight_3']['code_arrival'])->pluck('name')->first() }}
                                    </small>
                                </p>
                                
                                <div class="{{ $loop->first ? 'price-container' : ''}} d-flex align-items-center mt-3">
                                    <p class="price fs-4 m-0">{{ $flight['price'] }}€</p>
                                </div>
                                
                            </div>
                        </li>
                        @endforeach
                    </ol>
                    @else
                    <p><i class="fa-solid fa-circle-exclamation me-2"></i>Nessun volo con 2 scali</p>
                    @endif
                    
                </div>
            </div>
        </div>
        
        @else
        
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p class="display-6 m-0">Seleziona gli aeroporti di partenza e destinazione.</p>
                </div>
            </div>
        </div>
        
        @endif
    </main>
    
</body>
</html>